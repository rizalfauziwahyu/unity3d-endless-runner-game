﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{

    public GameObject _menuButtonGroup;
    public GameObject _playButton;
    public GameObject _scoreGroup;
    public GameObject _creditScreen;
    public GameObject _storeScreen;



    public void PlayGame()
    {
        SceneManager.LoadScene("Level");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void CreditButton()
    {
        _menuButtonGroup.gameObject.SetActive(false);
        _playButton.gameObject.SetActive(false);
        _scoreGroup.gameObject.SetActive(false);
        _creditScreen.gameObject.SetActive(true);
    }

    public void StoreButton()
    {
        _menuButtonGroup.gameObject.SetActive(false);
        _playButton.gameObject.SetActive(false);
        _scoreGroup.gameObject.SetActive(false);
        _storeScreen.gameObject.SetActive(true);
    }
    public void MainMenuButton()
    {
        _menuButtonGroup.gameObject.SetActive(true);
        _playButton.gameObject.SetActive(true);
        _scoreGroup.gameObject.SetActive(true);
        _creditScreen.gameObject.SetActive(false);
        _storeScreen.gameObject.SetActive(false);
    }
}
