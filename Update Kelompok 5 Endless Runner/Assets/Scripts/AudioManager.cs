﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public Sound[] sounds;


    public GameObject _soundButtonON;
    public GameObject _soundButtonOFF;
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.loop = s.loop;
        }

        PlaySound("MainTheme");
        
    }

    public void PlaySound(string name)
    {
        foreach (Sound s in sounds)
        {
            if (s.name == name)
                s.source.Play();
        }
    }

    public void MuteAllSound()
    {
        AudioListener.volume = 0;
        _soundButtonON.gameObject.SetActive(false);
        _soundButtonOFF.gameObject.SetActive(true);
    }

    public void UnMuteAllSound()
    {
        AudioListener.volume = 1;
        _soundButtonOFF.gameObject.SetActive(false);
        _soundButtonON.gameObject.SetActive(true);
    }
}
