using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{

    public GameObject _pauseScreen;
    public GameObject _pauseOnButton;
    public GameObject _pauseOffButton;


    public void MainMenuButton()
    {
        SceneManager.LoadScene("Menu");
    }

    public void PlayAgainButton()
    {
        _pauseScreen.gameObject.SetActive(false);
        _pauseOnButton.gameObject.SetActive(true);
        _pauseOffButton.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
    public void RestartButton()
    {
        SceneManager.LoadScene("Level");
    }

    public void PauseONButton()
    {
        _pauseScreen.gameObject.SetActive(true);
        _pauseOnButton.gameObject.SetActive(false);
        _pauseOffButton.gameObject.SetActive(true);
        Time.timeScale = 0;
    }
    public void PauseOFFButton()
    {
        _pauseScreen.gameObject.SetActive(false);
        _pauseOnButton.gameObject.SetActive(true);
        _pauseOffButton.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
}
